import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class NewsApiService {

    _API = environment.newsAPI_pt;

    constructor(private _http: HttpClient) { }

    topHeadlines(): Observable<any> {
        return this._http.get(this._API);
    }

}
