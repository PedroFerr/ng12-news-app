import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { NewsApiService } from '../news-api.service';
import { News } from '../news.interface';

@Component({
    selector: 'app-top',
    templateUrl: './top.component.html',
    styleUrls: ['./top.component.scss']
})
export class TopComponent implements OnInit {

    news$: Observable<News> | undefined;

    constructor(private api: NewsApiService) { }

    ngOnInit(): void {
        this.news$ = this.api.topHeadlines();
        // this.news$.subscribe(result => {
        //     console.log(result);
        // })
    }

}
