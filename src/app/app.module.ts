import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { TopComponent } from './news/top/top.component';

import { NewsApiService } from './news/news-api.service';
import { ShortnerPipe } from './pipes/shortner.pipe';

@NgModule({
    declarations: [
        ShortnerPipe,
        
        AppComponent,
        TopComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,

    ],
    providers: [NewsApiService],
    bootstrap: [AppComponent]
})
export class AppModule { }
